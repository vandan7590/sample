<?php

/*
 |--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//Display Dashboard
Route::get('dashboard/{name}',array('as'=>'dashboard','uses'=>'AdminController@dashboard'));
Route::get('home',array('as'=>'shoppy','uses'=>'HomeController@home'));
Route::post('home',array('as'=>'shoppy','uses'=>'HomeController@cat'));
Route::get('abouts',array('as'=>'about','uses'=>'HomeController@about'));
Route::get('faqs',array('as'=>'faqs','uses'=>'HomeController@faqs'));
Route::get('contacts',array('as'=>'contact','uses'=>'HomeController@contact'));
//contact form
Route::get('contacts',array('as'=>'contact','uses'=>'HomeController@contact'));
Route::post('contactform',array('as'=>'contactform','uses'=>'ContactController@store'));

//UserPanel
//Add Data (category)
Route::get('category/add',array('as'=>'category','uses'=>'CategoryController@add'));
Route::post('category/add',array('as'=>'category','uses'=>'CategoryController@store'));

//Edit Data (category)
Route::get('category/edit/{id}',array('as'=>'editcategory','uses'=>'CategoryController@edit'));
Route::post('category/edit/{id}',array('as'=>'editcategory','uses'=>'CategoryController@editstore'));

//View Data (category)
//Route::get('category/add',array('as'=>'viewcategory','uses'=>'CategoryController@view'));

//Delete Data (category)
Route::get('category/add/{id}',array('as'=>'deletecategory','uses'=>'CategoryController@destroy'));

//Product data add (Product)
Route::get('product/add',array('as'=>'productstore','uses'=>'ProductController@product'));
Route::post('product/add',array('as'=>'productstore','uses'=>'ProductController@store'));

//Product data view (Product)
Route::get('product/view',array('as'=>'viewproduct','uses'=>'ProductController@view'));

//Product data edit (Product)
Route::get('product/edit/{id}',array('as'=>'editproduct','uses'=>'ProductController@edit'));
Route::post('product/edit/{id}',array('as'=>'editproduct','uses'=>'ProductController@editstore'));

//Product image display (Product)
Route::get('product/image/{id}',array('as'=>'viewimage','uses'=>'ProductController@image'));

//Product data delete (Product)
Route::get('product/destory/{id}',array('as'=>'deleteproducts','uses'=>'ProductController@destroy'));
Route::get('product/deletes/{id}',array('as'=>'deleteproduct','uses'=>'ProductController@del'));

//signup form (Grocery)
Route::get('logins',array('as'=>'login','uses'=>'LoginController@login'));
Route::post('logins',array('as'=>'login','uses'=>'LoginController@loginstore'));

//login Form (Grocery)
Route::get('registration',array('as'=>'signup','uses'=>'LoginController@sign'));
Route::post('registration',array('as'=>'signup','uses'=>'LoginController@signstore'));

Route::get('dashboard/{name}',array('as'=>'userdashboard','uses'=>'UserController@dashboard'));

//Google map
Route::get('auto-city','HomeController@index')->name('auto-city');

//search
Route::post('search',array('as'=>'search','uses'=>'HomeController@search'));
//Route::post('search',array('as'=>'price','uses'=>'HomeController@search_range'));