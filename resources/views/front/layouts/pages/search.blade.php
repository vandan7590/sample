@extends('front.layouts.app')
@section('content')
{{--
<br><br><br><br>
@foreach($data as $datas)
    <form method="post">
        {{csrf_field()}}
        <center>
            <div class="box-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Product Price</label>
                    <br><font color="#8a2be2" size="3"><b> {{$datas->price}} </b> </font>
                </div>
            </div>
        </center>
    </form>
@endforeach
--}}
<div class="agileinfo-ads-display col-md-13">
    <div class="wrapper">
        <!-- first section (nuts) -->
        <div class="product-sec1">
            <h3 class="heading-tittle">Search Details</h3>
            @foreach($result as $results)
                <div class="product-men"> {{--col-md-4--}}
                    <div class="men-pro-item simpleCart_shelfItem">
                        <div class="men-thumb-item">
                            {{--<img src="{{asset('/images/vandan/'.$prods->image)}}" width="130" height="150">--}}
                            <label>Product Name</label>
                            <br><font color="#8a2be2" size="3"><b> {{$results->name}} </b> </font> <br><br>
                            <label for="exampleInputEmail1">Product Description</label>
                            <br><font color="#8a2be2" size="3"><b> {{$results->description}} </b> </font><br><br>
                            <label for="exampleInputEmail1">Product Price</label>
                            <br><font color="#8a2be2" size="3"><b> {{$results->price}} </b> </font>
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="clearfix"></div>
        </div>
    </div>
</div>
@endsection