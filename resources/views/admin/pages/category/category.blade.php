@extends('admin.layouts.app')
@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Admin Panel
        <small>Preview</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">Admin Panel</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <!--<div class="row">
    <!-- left column -->
    <!-- <div class="col-md-6">
        <!-- general form elements -->
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Category Form</h3>
        </div><!-- /.box-header -->
        <!-- form start -->
        @if($edit=="null")
        <form method="post" action="{{route('category')}}">
            {{csrf_field()}}
            <div class="box-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Category Name</label>
                    <input type="text" class="form-control"  placeholder="Enter Category Name" name="name">
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
        </div>
    <!-- Edit Data-->
    @else
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Edit Category Form</h3>
        </div><!-- /.box-header -->
        <!-- form start -->
        @foreach($edit as $edits)
            <form method="post" action="{{route('editcategory',['id'=>$edits->id])}}">
                {{csrf_field()}}
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Category Name</label>
                        <input type="text" class="form-control" value="{{$edits->name}}" name="name">
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        @endforeach
    </div>
    @endif
    <!-- Display Form-->
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Display Category Form</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered">
                        <tr>
                            <th style="width: 10px">ID</th>
                            <th>Name</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                        @foreach($cat as $cats)
                            <tr>
                                <td>{{$cats->id}}</td>
                                <td>{{$cats->name}}</td>
                                <td><a href="{{route('editcategory',['id'=>$cats->id])}}" class="btn btn-success"><i class="fa fa-edit"> Edit</i> </a> </td>
                                <td><a href="{{route('deletecategory',['id'=>$cats->id])}}" class="btn btn-danger"><i class="fa fa-trash"> Delete</i> </a> </td>
                            </tr>
                        @endforeach
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div>
    <!--Edit Data-->
</section><!-- /.content -->
@endsection