@extends('admin.layouts.app')
@section('content')
    <section class="content-header">
        <h1>
            Admin Panel
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Admin Panel</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Update Product Form</h3>
            </div>
            @foreach($edit as $edits)
            <form method="post" enctype="multipart/form-data" action="{{route('editproduct',['id'=>$edits->id])}}">
                {{csrf_field()}}
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Product Name</label>
                        <input type="text" class="form-control" value="{{$edits->name}}" placeholder="Enter Name" name="name">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Product Price</label>
                        <input type="text" class="form-control"  value="{{$edits->price}}" placeholder="Enter Price" name="price">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Product Description</label>
                        <input type="text" class="form-control"  value="{{$edits->description}}" placeholder="Enter Description" name="description">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Shipping</label>
                        <input type="text" class="form-control"  value="{{$edits->shipping}}" placeholder="Shipping Address" name="shipping">
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
            @endforeach
        </div>
@endsection