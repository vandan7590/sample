@extends('admin.layouts.app')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Display Product Form</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered">
                    <tr>
                        <th style="width: 10px">ID</th>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Description</th>
                        <th>Shipping</th>
                        <th>Edit</th>
                        <th>Delete</th>
                        <th>View Image</th>
                    </tr>
                    @foreach($product as $products)
                        <tr>
                            <td>{{$products->id}}</td>
                            <td>{{$products->name}}</td>
                            <td>{{$products->price}}</td>
                            <td>{{$products->description}}</td>
                            <td>{{$products->shipping}}</td>
                            <td><a href="{{route('viewimage',['id'=>$products->id])}}" class="btn btn-info"><i class="fa fa-file-photo-o"> View image </i> </a> </td>
                            <td><a href="{{route('editproduct',['id'=>$products->id])}}" class="btn btn-success"><i class="fa fa-edit"> Edit</i> </a> </td>
                            <td><a href="{{route('deleteproducts',['id'=>$products->id])}}" class="btn btn-danger"><i class="fa fa-trash"> Delete</i> </a> </td>
                            @endforeach
                        </tr>

                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div>
@endsection