@extends('admin.layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Display Product Form</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered">
                        <tr>
                            <th>ID</th>
                            <th>Product Name</th>
                            <th>Image</th>
                            <th></th>
                            <th></th>
                            </tr>
                        <tr>
                            @foreach($image as $images)
                                <td>{{$images->id}}</td>
                                <td>{{$images->prod->name}}</td>
                                <td><img src="{{asset('/images/vandan/'.$images->image)}}" height="60" width="60"></td>
                                <td><a href="{{route('deleteproduct',['id'=>$images->id])}}" class="btn btn-danger"><i class="fa fa-trash"> Delete </i> </a> </td>
                                <td><a href="{{route('viewproduct')}}" class="btn btn-primary"><i class="fa fa-angle-double-left"> Back </i> </a> </td>
                            </tr>
                            @endforeach
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div>
@endsection