<!DOCTYPE html>
<html>
<head>
    <title>Sign in and Sign up Form </title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="stylish Sign in and Sign up Form A Flat Responsive widget, Login form web template,Flat Pricing tables,Flat Drop downs  Sign up Web Templates, Flat Web Templates, Login signup Responsive web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!--online_fonts-->
    <link href="//fonts.googleapis.com/css?family=Sansita:400,400i,700,700i,800,800i,900,900i&amp;subset=latin-ext" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Poiret+One" rel="stylesheet">
    <!--//online_fonts-->
    <link href="{{asset('login/css/style.css')}}" rel='stylesheet' type='text/css' media="all" /><!--stylesheet-->
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <!-- Notification Script-->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
</head>
<body>
<h1>Sign in and Sign up form</h1>
<div class="form-w3ls">
    <div class="form-head-w3l">
        <h2>s</h2>
    </div>
    <ul class="tab-group cl-effect-4">
        <li class="tab"><a href="#signup-agile">Sign Up</a></li>
    </ul>
    <div class="tab-content">
        <div id="signup-agile">
            <form action="{{route('login')}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <p class="header">User Name</p>
                <input type="text" name="name" placeholder="Your Full Name" required="required">

                <p class="header">Email Address</p>
                <input type="email" name="email" placeholder="Email" required="required">

                <p class="header">Password</p>
                <input type="password" name="password" placeholder="Password" required="required">

                <p class="header">Confirm Password</p>
                <input type="password" name="confirm_password" placeholder="Confirm Password" required="required">

                <p class="header">Image Upload</p>
                <input type="file" name="image" required="required">

                <input type="submit" class="register" value="Sign up">
            </form>
        </div>
    </div><!-- tab-content -->
</div> <!-- /form -->
<p class="copyright">&copy; sign in and sign up Form. All Rights Reserved | Design by Vandan Makwana</p>
<!-- js files -->
<script src='{{asset('login/js/jquery.min.js')}}'></script>
<script src="{{asset('login/js/index.js')}}"></script>
<!-- /js files -->
<script>
            @if(Session::has('notification'))
            var type = "{{ Session::get('notification.alert-type', 'info') }}";
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('notification.message') }}");
            break;

        case 'warning':
            toastr.warning("{{ Session::get('notification.message') }}");
            break;
        case 'success':
            toastr.success("{{ Session::get('notification.message') }}");
            break;
        case 'error':
            toastr.error("{{ Session::get('notification.message') }}");
            break;
    }
    @endif
</script>
</body>
</html>
