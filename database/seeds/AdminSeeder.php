<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'=>'vandan',
            'email'=>'vandan@gmail.com',
            'password'=>bcrypt('1234'),
            'role'=>'1',
            'image'=>'0'
        ]);
    }
}
