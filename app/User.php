<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','image','role',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
         'remember_token',
    ];

    public static function loginUser($email,$password)
    {
        return \Auth::attempt(array('email'=>$email,'password'=>$password));
    }

    public function isAdmin()
    {
        return($this->role=='1');
    }

    public function isUser()
    {
        return($this->role=='2');
    }
}
