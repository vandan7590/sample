<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Prod extends Model
{
    protected $fillable=['name','price','description','shipping','cat_id','image'];

    public function prodimage()
    {
        return $this->hasMany('App\Prodimage');
    }
    public function categ()
    {
        return $this->belongsTo('App\Prod','cat_id');
    }

}
