<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prodimage extends Model
{
    protected $fillable=['product_id','name','image'];

    public function prod()
    {
        return $this->belongsTo('App\Prod','product_id');
    }

    public static function random()
    {
        return Prodimage::orderBy(\DB::raw('rand()'))->first();
    }
}
