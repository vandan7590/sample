<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categ extends Model
{
    protected $fillable = [
        'name',
    ];

    public function prod()
    {
        return $this->hasMany('App\Categ');
    }

}
