<?php

namespace App\Http\Controllers;

use App\Prod;
use Illuminate\Http\Request;
use App\Categ;
use App\Prodimage;
use Illuminate\Support\Facades\Input;
class HomeController extends Controller
{
    public function home()
    {
        $prod=Prod::limit(3)->get();
        $category=Categ::limit(4)->get();
        return view('front.layouts.pages.home',compact('prod','category'));
    }

    public function cat()
    {
        $category=Categ::all();
        return view('front.layouts.pages.home',compact('category'));
    }
    public function about()
    {
        return view('front.layouts.pages.about');
    }
    public function faqs()
    {
        return view('front.layouts.pages.faqs');
    }
    public function contact()
    {
        return view('front.layouts.pages.contact');
    }

    public function search(Request $request)
    {
        $name=$request->get('name');
        $result=\DB::table('prods')
            ->select(\DB::raw('*'))
            ->where('name','=',$name)
            ->get();
        return view('front.layouts.pages.search',compact('result'));
    }

    public function search_range(Request $request)
    {
        $price=$request->get('price');
        $data=\DB::table('prods')
            ->where(\DB::raw('price'))
            ->where('price','>=',$price)
            ->where('price','<=',$price)
            ->get();
        return view('front.layouts.pages.search',compact('data'));
    }
}
