<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categ;
class CategoryController extends Controller
{
    public function add()
    {
        $edit="null";
        $cat=Categ::all();
        return view('admin.pages.category.category',compact('edit','cat'));
    }
    public function store(Request $request)
    {
        $category=new Categ(array(
            'name'=>$request->get('name'),
        ));
        $category->save();
        return \Redirect::route('category');
    }
    public function edit($id)
    {
        $edit=Categ::where('id','=',$id)->get();
        $cat=Categ::all();
        return view('admin.pages.category.category',compact('edit','cat'));
    }
    public function editstore(Request $request,$id)
    {
        \DB::table('cats')
            ->where('id','=',$id)
            ->update(['name'=>$request->get('name')]);
        return \Redirect::route('category');
    }
    public function destroy($id)
    {
        $cat=Categ::find($id);
        $cat->delete();
        return \Redirect::route('category');
    }
}
