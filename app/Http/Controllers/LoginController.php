<?php

namespace App\Http\Controllers;
use App\Http\Requests\PasswordRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Mockery\Generator\StringManipulation\Pass\Pass;

class LoginController extends Controller
{
    public function login()
    {
        return view('login.login');
    }
    public function sign()
    {
        return view('login.register');
    }

    public function store(PasswordRequest $request)
    {
        $files=Input::file('image');
        $picName=str_random(30).'.'.$files->getClientOriginalExtension();
        $files->move(base_path() . '/public/images/login/', $picName);

        $register = new User(array(
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
            'image'=> $picName,
            'role'=>'2',
        ));
        $register->save();
        $notification = array(
            'message' => 'Successfully Register...',
            'alert-type' => 'success'
        );
        return \Redirect::route('shoppy')->with('notification',$notification);
    }
    public function loginstore(Request $request)
    {
        if(User::loginUser($request->email,$request->password))//Check UserName and Password
        {
            $email=$request->get('email');
            $role=User::where('email','=',$email)->pluck('role')->first();
            if ($role == '1')
            {
               $name=User::where('email','=',$email)->pluck('name')->first();
                return \Redirect::route('dashboard',['name'=>$name]);
            }
            elseif ($role == '2')
            {
                $name=User::where('email','=',$email)->pluck('name')->first();
                return \Redirect::route('userdashboard',['name'=>$name]);
            }
            else
            {
                return redirect()->back();
            }
        }
        else{
            return redirect()->back();
        }
    }
}
