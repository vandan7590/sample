<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
class ContactController extends Controller
{
    public function con()
    {
        return view('front.layouts.pages.contact');
    }
    public function store(Request $request)
    {
        $contact=new Contact(array(
            'name'=>$request->get('name'),
            'subject'=>$request->get('subject'),
            'email'=>$request->get('email'),
            'message'=>$request->get('message'),
        ));
        $contact->save();
        return \Redirect::route('contact');
    }
}
