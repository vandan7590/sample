<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Prod;
use App\Prodimage;
use App\Categ;
use Illuminate\Support\Facades\Input;
class ProductController extends Controller
{
    public function product()
    {
        $edit="null";
        $product=Prod::all();
        $category=Categ::all();
        return view('admin.product.add',compact('product','edit','category'));
    }
    
    public function store(Request $request)
    {
        $product=new Prod(array(
            'name'=>$request->get('name'),
            'price'=>$request->get('price'),
            'description'=>$request->get('description'),
            'shipping'=>$request->get('shipping'),
            'cat_id'=>$request->get('category'),
            'image'=>'-',
        ));
        $product->save();
        $id=$product->id;

        foreach((Input::file('image'))as $files)
        {
            $picName=str_random(30).'.'.$files->getClientOriginalExtension();
            $files->move(base_path().'/public/images/vandan/',$picName);

            /**/

            \DB::table('prods')->where('id','=',$id)
                ->update(['image'=>$picName]);

            $data=new Prodimage(array(
                'product_id'=>$id,
                'image'=>$picName,
            ));
            $data->save();
        }
        return \Redirect::route('productstore');
    }
    public function view()
    {
        $image=Prodimage::all();
        $product=Prod::all();
        return view('admin.product.view',compact('product','image'));
    }

    public function edit($id)
    {
        $edit=Prod::where('id','=',$id)->get();
        $product=Prod::all();
        return view('admin.product.edit',compact('product','edit'));
    }
    public function editstore(Request $request,$id)
    {
        \DB::table('products')
            ->where('id','=',$id)
            ->update([
                'name'=>$request->get('name'),
                'price'=>$request->get('price'),
                'description'=>$request->get('description'),
                'shipping'=>$request->get('shipping'),
            ]);
        return \Redirect::route('viewproduct');
    }

    public function destroy($id)
    {
        $img =Prodimage::where('cat_id','==',$id)->pluck('id')->first();
        if(isset($img))
        {
            echo"Please Delete Images.....!";
            return redirect()->back();
        }
        else
        {
            $data=Prod::find($id);
            $data->delete();
            return redirect()->back();
        }

    }
    public function image($id)
    {
        $image=Prodimage::where('Product_id','=',$id)->get();
        return view('admin.product.viewimage',compact('image'));
    }
    public function del($id)
    {
        $product = Prodimage::where('cat_id',$id)->first();
        $product->delete();
        return \Redirect::route('viewproduct');
    }
}

