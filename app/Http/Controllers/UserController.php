<?php

namespace App\Http\Controllers;

use App\User;
use App\Cat;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function dashboard($name)
    {
        $data=User::where('name','=',$name)->get();
        return view('user.pages.dashboard.dashboard',compact('data','name'));
    }
}
